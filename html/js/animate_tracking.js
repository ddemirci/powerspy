var SOUTH_BAY_LAT = 37.38;
var SOUTH_BAY_LON = -122;

var TRACKING_DATA_URL = 'data/tracking_dtw-20140619_190335.csv';
var START_MARKER_IMAGE = 'images/startmarker.png';
var TARGET_CAR_IMG = 'images/car.png';
var EST_IMG = 'images/target.png';
var ANIMATION_DELAY = 50;

// colors can be specified by either names or RGB numbers
// such as '#FF0000'
var DEFAULT_COLOR = 'blue';

var map = null;
var mapCenter = null;
var lastData = null;

var lastPath = null;

TIME = 0;
TRUE_LAT = 2;
TRUE_LON = 1;
EST_LAT = 4;
EST_LON = 3;

function init_last_path() {
    return {
        lines: [],
        startMarker: null,
        endMarker: null
    };
}

function value_mapping(value) {
    return Math.log(value);
}

function find_min_max(values) {
    var minval = Infinity;
    var maxval = -Infinity;

    for (var i = 0; i < values.length; ++i) {
        if (values[i] < minval) {
            minval = values[i];
        }

        if (values[i] > maxval) {
            maxval = values[i];
        }
    }

    return [minval, maxval];
}

function find_values_range(values) {
    var min_max = find_min_max(values);
    return min_max[1] - min_max[0];
}

/*
  Get latitude/longitude coordinates from Json data.
 */
function get_path_coords(data) {
    var pathCoords = [];

    for (var i = 0; i < data.length; i++) {
        var latitude = data[i][TRUE_LAT]; // TrueLat
        var longitude = data[i][TRUE_LON]; // TrueLon

        pathCoords.push(new google.maps.LatLng(latitude, longitude));
    }

    return pathCoords;
}

/*
  Get all values for a given column.
*/
function get_column_values(rows, colName) {
    // $.map doesn't work for very large data arrays
    values = [];
    for (var i = 0; i < rows.length; ++i) {
        values.push(rows[i][colName]);
    }

    return values;
} // end of get_column_values

/*
 Calculate the coordinates of the center of the path
 described by the Json data.
 */
function get_path_center(data) {
    if (0 == data.length) {
        console.warn("Empty data");
        return null;
    }

    var latValues = get_column_values(data, TRUE_LAT);
    var lonValues = get_column_values(data, TRUE_LON);

    var lat_min_max = find_min_max(latValues);
    var latMax = lat_min_max[1];
    var latMin = lat_min_max[0];
    var lon_min_max = find_min_max(lonValues);
    var lonMax = lon_min_max[1];
    var lonMin = lon_min_max[0];

    var centerLat = (latMax + latMin) / 2;
    var centerLon = (lonMax + lonMin) / 2;

    return {
        lat: centerLat,
        lon: centerLon
    };
} // end of get_path_center

function create_map() {
    var map_canvas = document.getElementById('map_canvas');

    var map_options = {
        center: new google.maps.LatLng(SOUTH_BAY_LAT, SOUTH_BAY_LON),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    return new google.maps.Map(map_canvas, map_options);
} // end of create_map

function put_marker(map, coords, title, icon) {
    var marker = new google.maps.Marker({
        position: coords,
        map: map,
        title: title
    });

    if (icon) {
        marker.setIcon(icon);
    }

    return marker;
} // end of put_marker

function draw_path_part(map, pathCoords, color) {
    var path = new google.maps.Polyline({
        path: pathCoords,
        geodesic: true,
        strokeColor: color,
        strokeOpacity: 1,
        strokeWeight: 5,
        map: map
    });

    return path;
} // end of draw_path_part

function get_column_min_max(data, field) {
    var filteredData = jQuery.grep(data, function(row) {
        return row[field] != -1;
    });

    var values = get_column_values(filteredData, field);
		var min_max = find_min_max(values);

    return {
        min: min_max[0],
        max: min_max[1]
    };
} // end get_column_min_max

var i = 0;
var targetMarker = null;
var estMarker = null;

function animate_tracking_aux(map, data) {
    if (i == data.length) {
				i = 0;
    }
		else {
			var targetCoords = 
					new google.maps.LatLng(data[i][TRUE_LAT], data[i][TRUE_LON]);
			
			var estCoords = 
					new google.maps.LatLng(data[i][EST_LAT], data[i][EST_LON]);
			
			if (null != targetMarker) {
					targetMarker.setPosition(targetCoords);
			} else {
				targetMarker = put_marker(map, targetCoords, "Target", TARGET_CAR_IMG);
			}
			
			if (null != estMarker) {
					estMarker.setPosition(estCoords);
			} else {
				estMarker = put_marker(map, estCoords, "Estimate", EST_IMG);
			}
			
			++i;
		}

    setTimeout(function() { animate_tracking_aux(map, data); }, ANIMATION_DELAY);
} // end of animate_tracking_aux

function animate_tracking(map, data) {
    i = 0;
    animate_tracking_aux(map, data);
} // end of animate_tracking

/*
  Visualize data on map. Color the path line according to
  the values of a field specified by COLOR_FIELD.
 */
function visualize_data(map, data) {
    var color = DEFAULT_COLOR;
    var pathCoords = get_path_coords(data);
    if (0 == pathCoords.length) {
        console.warn("Empty data");
        return;
    }

    /*
    var startMarkerCoords =
        new google.maps.LatLng(pathCoords[0].k, pathCoords[0].D);

    var endMarkerCoords =
        new google.maps.LatLng(pathCoords[pathCoords.length - 1].k,
            pathCoords[pathCoords.length - 1].D);

    lastPath.startMarker = put_marker(map, startMarkerCoords, "Start", START_MARKER_IMG);
    lastPath.endMarker = put_marker(map, endMarkerCoords, "Finish");
    */

    lastPath.lines.push(draw_path_part(map, pathCoords, DEFAULT_COLOR));
    
    animate_tracking(map, data);
} // end of visualize_data

/*
  Process data in JSON format and show on map.
 */
function process_data(data) {
    
    if (null == map) {
        // no map created yet, let's create one
        map = create_map();
    }

    var pathCenter = get_path_center(data);
    if (null != pathCenter) {
        mapCenter = pathCenter;
        map.setCenter(new google.maps.LatLng(mapCenter.lat, mapCenter.lon));
    }

    visualize_data(map, data);
}

function csv2json(csvData) {
    return csvjson.csv2json(csvData, {
        delim: ",",
        header: false
    });
}

function process_csv_data(data) {
    var jsonObj = csv2json(data);
    // jsonObj.headers = ["Time", "TrueLon", "TrueLat", "EstLon", "EstLat"];

    lastPath = init_last_path();
    lastData = jsonObj.rows;

    process_data(lastData);
} // end process_csv_data

/*
 Fetches CSV data from URL, converts to JSON
 and shows on the map.
 */
function load_data(url) {
    $.ajax(url, {
        success: function(data) {
            console.log("Loaded CSV data");
            process_csv_data(data);
        },
        
        error: function() {
            console.error("Failed loading data from " + url);
        }
    });
} // end of load_data

/*
 Fit map canvas size to window size.
 */
function fit_map() {
    $('#map_canvas').height($(map_container).height());
    $('#map_canvas').width($(map_container).width());
}

function handle_drag(e) {
    e.stopPropagation();
    e.preventDefault();
    document.getElementById('drop_container').style.display = 'block';
    return false;
}

function handle_file(file) {
    var reader = new FileReader();
    reader.onload = function(e) {
        process_csv_data(e.target.result);
    };

    reader.onerror = function(e) {
        console.error('Failed reading file ' + file.name);
    }

    reader.readAsText(file);
} // end of handle_file

function handle_drop(e) {
    e.preventDefault();
    e.stopPropagation();

    var files = e.dataTransfer.files;
    if (0 == files.length) {
        // no files to process
        // propagate event
        return true;
    }

    for (var i = 0; i < files.length; i++) {
        handle_file(files[i]);
    }

    // prevent event propagation
    return false;
} // end of handle_drop

/*
 Set callback functions to handle drag-over and drop events.
 */
function init_events() {
    var map_canvas = document.getElementById("map_canvas");
    var drop_container = document.getElementById("drop_container");

    [map_canvas, drop_container].forEach(function(container) {
        container.addEventListener('drop', handle_drop, false);
        container.addEventListener('dragover', handle_drag, false);
    });

    $("select").change(function() {
        var option = $("select option:selected").text();
        on_combo_change(option);
    });
}

function initialize() {
    init_events();
    
    mapCenter = {
        lat: SOUTH_BAY_LAT,
        lon: SOUTH_BAY_LON
    };

    lastPath = init_last_path();

    // fit map canvas to window size
    $(window).resize(function() {
        fit_map();
        if (null != map) {
            map.setCenter(new google.maps.LatLng(mapCenter.lat, mapCenter.lon));
        }
    });

    fit_map();
    create_map(null);
    
    load_data(TRACKING_DATA_URL);
} // end of initialize

google.maps.event.addDomListener(window, 'load', initialize);
google.load("visualization", "1", { packages:["corechart"]});
