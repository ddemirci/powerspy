#!/usr/bin/env python

# Set route labels according to route equivalences

import sys
import pandas

def get_routes_dict(equiv_filename):
    route_dict = {}
    i = 1
    with open(equiv_filename) as f:
        for l in f:
            for route in l.split():
                route_dict[route] = i
            i += 1
    return route_dict

def set_labels(log_list, route_dict):
    log_list['Label'] = log_list.apply(lambda row: route_dict[row.Route], 1)

def main():
    loglist_filename = sys.argv[1]
    equiv_filename = sys.argv[2]
    output_file = sys.argv[3]

    route_dict = get_routes_dict(equiv_filename)
    print '%d routes' % len(route_dict.keys())

    log_list = pandas.read_table(loglist_filename)
    set_labels(log_list, route_dict)

    log_list.to_csv(output_file, index=False, sep='\t')

if __name__ == '__main__':
    main()
