#!/usr/bin/env python

# Get a subset of routes for classification validation

import sys
import random
import pandas

def get_log_subset(log_list, num_per_class):
    indices = []
    for l in set(log_list['Label']):
        # choose random subset for that label
        idx = log_list.index[log_list['Label'] == l]
        if len(idx) < num_per_class:
            print 'Skipping', l, '. Not enough samples.'
            continue
        chosen_idx = random.sample(idx, num_per_class)
        indices.extend(chosen_idx)
    return log_list.loc[indices]

def main():
    loglist_file = sys.argv[1]
    num_per_class = int(sys.argv[2])
    output_file = sys.argv[3]

    log_list = pandas.read_table(loglist_file)
    log_subset = get_log_subset(log_list, num_per_class)
    log_subset.to_csv(output_file, index=False, sep='\t')

if __name__ == '__main__':
    main()
