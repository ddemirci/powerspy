function analyze_tracking(csvdatafile)
    
    data = reshape(csvread(csvdatafile), [], 5);
    N = size(data, 1);
    est_error = zeros(N, 1);
    
%     time = data(:, 1);
    true_lon = data(:, 2);
    true_lat = data(:, 3);
    est_lon = data(:, 4);
    est_lat = data(:, 5);
    
    for i = 1:N
        est_error(i) = gps_to_distance(struct('lon', est_lon(i), 'lat', est_lat(i)), ...
            struct('lon', true_lon(i), 'lat', true_lat(i)));
    end
    
%     figure;
%     plot(time / 1000, est_error * 1000);
%     xlabel('Time [seconds]');
%     ylabel('Estimation error [meters]');
%     set(gca, 'box', 'off');
    
    figure;
    [nerrors, err_centers] = hist(est_error * 1000, 100);
    bar(err_centers, nerrors, 'c');
    axis tight;
    xlabel('Estimation error [meters]', 'FontSize', 15);
    set(gca, 'box', 'off');
    
    cum_error = cumsum(nerrors);
    cum_error = cum_error / cum_error(end);
    figure;
    bar(err_centers, cum_error, 'c');
    xlabel('Estimation error [meters]', 'FontSize', 15);
    ylabel('Error CDF', 'FontSize', 15);
    set(gca, 'box', 'off');
end
