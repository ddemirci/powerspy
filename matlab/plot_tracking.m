function plot_tracking(h, true_lon, true_lat, est_lon, est_lat, dtw_lon, dtw_lat, profile, locked)
    figure(h);
    scatter(profile(:, 2), profile(:, 3), 'fill');
    hold all;
    
    scatter(true_lon, true_lat, 300, 'fill');
    
    scatter(dtw_lon, dtw_lat, 200, 'blue', 'fill');
    
    % plot location guess
    if locked
        scatter(est_lon, est_lat, 100, 'green', 'fill');
    else
        scatter(est_lon, est_lat, 100, 'fill');
    end
    hold off;
    legend('Route', 'True location', 'Estimated location');
end