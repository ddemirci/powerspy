function [est_error, time, true_lon, true_lat, est_lon, est_lat] = ...
    test_tracking_using_mult_profiles(test_device, test_filename, step_size)
    
    LOG_DIR = '../../BatLevelLogs';
   
    DS_FACTOR = 10;
    EST_ERROR_THRESHOLD = step_size / (100/DS_FACTOR);
    ERROR_CORRECTION = false;
    PLOT_DATA = true;
    RECORD_MOVIE = false;
    KALMAN = false;
    % Set true to use different start index
    TOGGLE_START_INDEX = false; 
    % Set 'dtw' or 'osb'
    ALGORITHM = 'dtw';
    FPS = 10;
    
    WINDOW_SIZE = 2000;
    
    assert(PLOT_DATA || ~RECORD_MOVIE);
    
    log_list = get_log_list(test_device, test_filename);
    test_file = [LOG_DIR '/' test_device '/' test_filename];
    
    test_data = read_data(test_file);
    test_profile = create_profile(test_data, DS_FACTOR);
    
    SCORE_THRESHOLD = 1;
    
    ind_range = 1:floor(length(test_profile)/step_size);
    time = zeros(size(ind_range));
    
    if RECORD_MOVIE
        movie_frames(length(ind_range)) = struct('cdata',[],'colormap',[]);
    end
    
    est_error = zeros(size(ind_range));
    true_lon = zeros(size(ind_range));
    true_lat = zeros(size(ind_range));
    est_lon = zeros(size(ind_range));
    est_lat = zeros(size(ind_range));
    score = zeros(size(ind_range));
    
    %%% Kalman
    if KALMAN
        kalman_filter = KalmanFilter([0 0]);
        filter_not_ready = true;
    end
    %%%
    
    if PLOT_DATA
        error_plot = figure;
        tracking_plot = figure;
    end
    
    locked = false;
    misestimation_counter = 0;
    MISESTIMATE_THRESHOLD = 5;
    
    progressbar;
    
    start_index = ones(length(log_list),1);
    for i = ind_range        
        % Modified pos from 1+(i-1)step_size so that it gives valid start time with dtw.
        % It usually fails to give correct start time when subseq size is 1
        position = i*step_size; 
        fprintf('Position: %d\n', position);
        time(i) = test_profile(position, 1);

        true_lon(i) = test_profile(position, 2);
        true_lat(i) = test_profile(position, 3);

        window_size = 2000;
        init_position = 1;%max(1, position - window_size);
        subseq = test_profile(1:position, 4);
        subseq = preprocess_data(subseq, 'SmoothingWindow', 200);
        subseq = subseq(init_position:position, 1);
        
        [est_lon(i), est_lat(i), score(i), start_index_new] = find_most_likely_loc( ... 
            log_list, subseq, DS_FACTOR, start_index, ALGORITHM);
        if TOGGLE_START_INDEX && position > window_size
            start_index = start_index_new;
        end
        
        % Used for plotting the estimate just based on dtw and not Kalman
        dtw_lon = est_lon(i); 
        dtw_lat = est_lat(i);
        
        if i > 1
            displacement = gps_to_distance(struct('lon', est_lon(i), 'lat', est_lat(i)), ...
                struct('lon', est_lon(i-1), 'lat', est_lat(i-1)));
            
            fprintf('Est. location: %f, %f\tScore: %f\tDisp: %f\n', ...
                est_lat(i), est_lon(i), score(i), displacement);
        end

        if ~KALMAN
            if locked && (displacement > EST_ERROR_THRESHOLD) && ERROR_CORRECTION
               fprintf('Unlikely estimation\n');
               est_lon(i) = est_lon(i - 1);
               est_lat(i) = est_lat(i - 1);
               misestimation_counter = misestimation_counter + 1;
               if misestimation_counter >= MISESTIMATE_THRESHOLD
                   fprintf('Unlocked\n');
                   locked = false;
                   misestimation_counter = 0;
               end
            elseif score(i) < SCORE_THRESHOLD && ~locked
               fprintf('Locked\n');
               locked = true;
            end

            if score(i) >= SCORE_THRESHOLD && locked
               fprintf('Target lost\n'); 
               locked = false;
            end
        end
        
        %%% Kalman
        if locked && KALMAN
            if filter_not_ready
                kalman_filter.set_initial_state(convert_gps_to_xy(est_lon(i), est_lat(i)), ...
                    convert_gps_to_xy(est_lon(i-1), est_lat(i-1)));
            end
            [est_x,est_y] = kalman_filter.estimate_loc;
            [est_lon(i), est_lat(i)] = convert_xy_to_gps(est_x, est_y);
        end
        %%%
        
        est_error(i) = gps_to_distance(struct('lon', est_lon(i), 'lat', est_lat(i)), ...
            struct('lon', true_lon(i), 'lat', true_lat(i)));
        
        if PLOT_DATA
            plot_tracking(tracking_plot, true_lon(i), true_lat(i), ...
                est_lon(i), ...
                est_lat(i), ...
                dtw_lon, ...
                dtw_lat, ...
                test_profile, ...
                locked);
            
            figure(error_plot)
            subplot(211);
            plot(est_error);
            subplot(212);
            plot(score(score < prctile(score, 90)));
        end
        
        if RECORD_MOVIE
            movie_frames(i) = getframe(tracking_plot);
        end
        
        progressbar(i/ind_range(end));
    end
    
    if RECORD_MOVIE
        movie2avi(movie_frames, 'tracking.avi', 'FPS', FPS);
    end
    
    output_file = ['tracking_' test_filename];
    if ERROR_CORRECTION
        output_file = [output_file '.improved'];
    end
    dlmwrite(output_file, [time' true_lon' true_lat' est_lon' est_lat'], ...
        'delimiter', ',', 'precision', 9);
    
    figure(3);
    hold
    plot(time / 1000, est_error * 1000);
    axis tight;
    xlabel('Time [seconds]', 'FontSize', 15);
    ylabel('Estimation error [meters]', 'FontSize', 15);
    set(gca, 'box', 'off');
    
    figure;
    hist(est_error * 1000, 50);
    axis tight;
    xlabel('Estimation error [meters]', 'FontSize', 15);
    set(gca, 'box', 'off');
end

function [est_lon, est_lat, score, start_index_new] = find_most_likely_loc(log_list, subseq, ds_factor, start_index, alg)
    distance_vector = inf(length(log_list), 3);
    
    for i = 1:length(distance_vector)
        data = preprocess_data(log_list.Data{i}.Power, ...
            'SmoothingWindow', 200, 'DownsampleFactor', ds_factor);
        [distance_vector(i, 1), distance_vector(i, 2), distance_vector(i, 3)] = ...
            search_subsequence(data(start_index(i,1):end,1), subseq, alg);
        distance_vector(i,2) = distance_vector(i,2) + start_index(i,1) - 1;
        distance_vector(i,3) = distance_vector(i,3) + start_index(i,1) - 1;        
    end
    
    display(distance_vector(:,2));
    [score, profile_ind] = min(distance_vector(:, 1));
    offset = min(distance_vector(profile_ind, 3), ...
        length(log_list.Data{profile_ind}.Power)/ds_factor);
    offset = offset * ds_factor;
    est_lon = log_list.Data{profile_ind}.Longitude(offset);
    est_lat = log_list.Data{profile_ind}.Latitude(offset);
    start_index_new = ceil(distance_vector(:,2)/2);
end

function profile = create_profile(data, ds_factor)
    preprocessed_power = preprocess_data(data.Power, ...
        'SmoothingWindow', 200);
    profile = [data.RelativeTime data.Longitude data.Latitude preprocessed_power];
    profile = downsample(profile, ds_factor);
end

function log_list = get_log_list(test_device, test_filename)
    [log_list, ~] = read_log_list('../../BatLevelLogs/tosb_logs.csv');
    
    log_filter = ~strcmp(log_list.Filename, test_filename) ...
        & strcmp(log_list.Current, 'Yes') ...
        & ~strcmp(log_list.Charging, 'Yes') ...
        & strcmp(log_list.Route, 'tosb') ...
        & ~strcmp(log_list.Device, test_device);

    log_list = log_list(log_filter, :);
    fprintf('%d entries in filtered log list\n', length(log_list));
end
