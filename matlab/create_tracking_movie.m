function create_tracking_movie(test_file, csvdatafile)
    FPS = 10;
    
    test_data = read_data(test_file);
    test_profile = create_path(test_data);
    
    data = csvread(csvdatafile);
    N = size(data, 1);
    
    true_lon = data(:, 2);
    true_lat = data(:, 3);
    est_lon = data(:, 4);
    est_lat = data(:, 5);
    
    movie_frames(N) = struct('cdata',[],'colormap',[]);
    
    for i = 1:N
        plot_tracking(gcf, true_lon(i), true_lat(i), ...
            est_lon(i), ...
            est_lat(i), ...
            test_profile);
        
        movie_frames(i) = getframe(gcf);
    end
    
    movie2avi(movie_frames, 'tracking.avi', 'FPS', FPS);
end

function profile = create_path(data)
    profile = [data.RelativeTime data.Longitude data.Latitude];
    profile = downsample(profile, 100);
end