function data_out = smooth_data(data_in, smoothing_window)
    % high smoothing factor to remove outliers in addition to noise
    data = smooth(data_in, smoothing_window);
    data_out = remove_dc_offset(data);
end