function data = get_data_for_route(log_list, route_name, varargin)
    data_filter = strcmp(log_list.Route, route_name);
    
    % filter by device
    if nargin > 2
        device = varargin{1};
        device_filter = strcmp(device, filtered_loglist);
        data_filter = data_filter && device_filter;
    end
    
    data = log_list.Data(data_filter);
end