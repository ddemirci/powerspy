function class = dtw_classify_mean_distance(i, distance_matrix, train_ind, train_labels)
    % classification using mean distance
    label_set = unique(train_labels);
    label_score = nan(length(label_set), 1);

    for j = 1:length(label_set)
        idx_to_sum = train_ind(strcmp(train_labels, label_set{j}));
        label_score(j) = mean(distance_matrix(i, idx_to_sum));
    end

    [~, ind] = min(label_score);
    assert(train_ind(ind) ~= i);
    class = train_labels{ind};
end
