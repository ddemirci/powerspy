function mcr = test_dtw_classification(loglist_filename)
    [log_list, ~] = read_log_list(loglist_filename);
    
    filter_field_name = 'Current';

    % filter devices that support signal strength measurement
    log_filter = strcmp(log_list.(filter_field_name), 'Yes') & ...
                 ~strcmp(log_list.Charging, 'Yes');
    
    log_list = log_list(log_filter, :);
%     log_list = cell2table(filtered_log_list, 'VariableNames', ...
%         log_list.Properties.VariableNames);
    fprintf('%d entries in filtered log list\n', length(log_list));
    display(unique(log_list.Route));
    fprintf('%d unique routes\n', length(unique(log_list.Route)));
    
    mcr = evaluate_dtw_classification(log_list);
end