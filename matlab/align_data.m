function output_data = align_data(input_data, pflag, varargin)
    num_inputs = length(input_data);
    output_series = cell(num_inputs, 1);
    for i = 2:num_inputs
        [~, ~, ~, ~, output_series{1}, output_series{i}] = ...
            dtw(input_data{1}, input_data{i}, pflag);
    end
    
    if pflag
        figure;
        for i = 1:num_inputs
            plot(output_series{i});
            hold all;
        end
        title('Aligned data');
    end
    
    output_data = output_series;
end