function [d, a, b] = dtw_subsequence(X, Y, varargin)
    % X - subsequence, each sample in a row
    % Y - longer sequence, each sample in a row
    % w - (optional) DTW window size
    
    w = Inf;
    if nargin > 2
        w = varargin{1};
    end
    
    N = length(X);
    M = length(Y);
    if size(X, 2)~=size(Y, 2)
        error('Error in dtw_subsequence(): the dimensions of the two input signals do not match.');
    end
    w=max(w, abs(N - M)); % adapt window size

    % initialization
    D = inf(N + 1, M + 1); % cache matrix
    D(1, M) = 0;
    
    % subsequence initialization (Muller07)
    cost = inf(N, 1);
    for k = 1:N
        cost(k) = norm(X(k, :) - Y(1, :));
    end
    D(1:end-1, 1) = cumsum(cost);
    
    cost = inf(M, 1);
    for m = 1:M
        cost(m) = norm(X(1, :) - Y(m, :));
    end
    D(1, 1:end-1) = cost;
    
    % begin dynamic programming
    for i = 1:N
        for j = max(i - w, 1):min(i + w, M)
            cost = norm(X(i, :)- Y(j, :));
            D(i+1, j+1) = cost + min( [D(i, j+1), D(i+1, j), D(i, j)] );
        end
    end
    
%     display(D);
    
    [d, b] = min(D(N + 1, :));
    
    % find a through optimal warping path
    p = optimal_warping_path(D, [N+1, b]);
    p = flip(p);
%     display(p);
    
    a = find(p(:, 1) == 1, 1, 'last');
end

function p = optimal_warping_path(D, p)
    % D - accumulated cost matrix
    % p - initial path
    
    n = p(end, 1);
    m = p(end, 2);
    while ~((n == 1) && (m == 1))
        if n == 1
            pl_prev = [1, m-1];
        elseif m == 1
            pl_prev = [n-1, 1];
        else
            sub_ind = [ [n-1, m-1]; [n, m-1]; [n-1, m] ];
            ind = sub2ind(size(D), sub_ind(:, 1), sub_ind(:, 2));
            [~, min_ind] = min(D(ind));
            [i, j] = ind2sub(size(D), ind(min_ind));
            pl_prev = [i j];
        end

        p = [p; pl_prev];
        n = p(end, 1);
        m = p(end, 2);
    end
end
