function calc_hist_for_tracking_data
    data_files = {'tracking_tosb-20140619_190335.csv', ...
			'tracking_tosb-20140623_131057.csv', 'tracking_tosb-20140708_201641.csv', ...
			'tracking_tosb-20140626_145409.csv'};

    data = [];
    for i = 1:length(data_files)
        new_data = csvread(data_files{i});
        data = [data; new_data];
    end

%     time = data(:, 1);
    true_lon = data(:, 2);
    true_lat = data(:, 3);
    est_lon = data(:, 4);
    est_lat = data(:, 5);

    N = size(data, 1);
    est_error = zeros(N, 1);
    for i = 1:N
        est_error(i) = gps_to_distance(struct('lon', est_lon(i), 'lat', est_lat(i)), ...
            struct('lon', true_lon(i), 'lat', true_lat(i)));
    end

    figure;
    [nerrors, err_centers] = hist(est_error * 1000, 100);
    bar(err_centers, nerrors, 'c');
    axis tight;
    xlabel('Estimation error [meters]', 'FontSize', 15);
    set(gca, 'box', 'off');

    cum_error = cumsum(nerrors);
    cum_error = cum_error / cum_error(end);
    figure;
    bar(err_centers, cum_error, 'c');
    xlabel('Estimation error [meters]', 'FontSize', 15);
    ylabel('Error CDF', 'FontSize', 15);
    set(gca, 'box', 'off');
end
