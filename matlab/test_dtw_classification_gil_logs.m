function mcr = test_dtw_classification_gil_logs
    [log_list, ~] = read_log_list('gil_logs.csv');
    
    filter_field_name = 'Current';

    % filter devices that support signal strength measurement
    log_filter = strcmp(log_list.(filter_field_name), 'Yes') & ...
        (strcmp(log_list.Route, 'gil-to-work-alma') | ...
         strcmp(log_list.Route, 'gil-from-work-alma'));

     
    filtered_log_list = log_list{log_filter, :};
    log_list = cell2table(filtered_log_list, 'VariableNames', ...
        log_list.Properties.VariableNames);
    fprintf('%d entries in filtered log list\n', height(log_list));
    
    mcr = evaluate_dtw_classification(log_list);
end