function test_kalman_filter

    init_state.x = 0;
    init_state.y = 0;
    
    kalman_filter = KalmanFilter([init_state.x, init_state.y]);

    num_samples = 20;

    est_x = zeros(1,num_samples);
    est_y = zeros(1,num_samples);
    
    observation = repmat(struct('x',3,'y',5), num_samples, 1 );
    for i = 1:10
        observation(i).x = 2*i;
        observation(i).y = 6*i;
    end
    
    for i = 11:20
        observation(i).x = 3*i - 10;
        observation(i).y = 3*i + 30;
    end

    for i = 1:num_samples
        [est_x(i), est_y(i)] = kalman_filter.estimate_loc([observation(i).x, observation(i).y]);
    end
    
    est_x
    est_y

end