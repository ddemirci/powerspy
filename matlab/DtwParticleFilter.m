classdef DtwParticleFilter < ParticleFilter
    properties (Constant)
        WINDOW_SIZE = 10;
    end
    
    properties (Access = private)
       measurements; 
    end
    
    methods
       function obj = DtwParticleFilter(data, num_particles)
           % Class constructor
           obj@ParticleFilter(data, num_particles);
           obj.measurements = zeros(obj.WINDOW_SIZE, 1);
       end
    end
    
    methods (Access = protected)
        
        function distance = calc_distance(obj, new_measurement)
            num_particles = length(obj.particles);
            obj.measurements = [obj.measurements(2:end, :); new_measurement];
            distance = zeros(num_particles, 1);
            
            for i = 1:num_particles
                data_ind = obj.particles(i);
                if data_ind > obj.WINDOW_SIZE
                    ref_wnd = obj.data(data_ind - obj.WINDOW_SIZE, :);
                else
                    ref_wnd = obj.data(1:data_ind, :);
                end
                distance(i) = get_dtw_distance(ref_wnd, obj.measurements) / 100;
            end
        end
        
    end
end