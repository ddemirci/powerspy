function plot_loglist_data(log_list, field_name)
    subplot(211);
    preprocessed_data = cell(height(log_list), 1);
    for i = 1:height(log_list)
        preprocessed_data{i} = preprocess_data(log_list.Data{i}.(field_name), ...
            'SmoothingWindow', 100, 'DownsampleFactor', 20);
        plot(preprocessed_data{i});
        hold all;
    end
    
    aligned_data = align_data(preprocessed_data, false);
    
    subplot(212);
    for i = 1:length(aligned_data)
        plot(aligned_data{i});
        hold all;
    end
end