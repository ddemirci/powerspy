package com.securitylab.getbatterylevel;

import java.lang.ref.WeakReference;

import com.securitylab.getbatterylevel.R;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

public class Main extends Activity implements CompoundButton.OnCheckedChangeListener {
	private static final String PREF_EXECUTION_STATUS = "ExecutionStatus";

	// time to wait before stopping the background service upon stop
	private static final int PRE_STOP_WAIT = 200;
	
	// UI controls
	private TextView m_contentTxt;
	private CheckBox cbGPS;
	private CheckBox cbSignalStrength;
	private CheckBox cbBattery;
	private CheckBox cbSaveLog;
	private EditText m_commentTxt;
	private Switch m_startStopSwitch;
	
	// Receives messages from the background service
	protected static IncomingHandler m_inhandler;
	
	private ServiceConnection m_serviceConnection;

	private boolean isMyServiceRunning(Class<?> serviceClass) {
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (serviceClass.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		m_contentTxt = (TextView) findViewById(R.id.batterycharge);
		cbGPS = (CheckBox) findViewById(R.id.gpsCheckBox);
		cbSignalStrength = (CheckBox) findViewById(R.id.signalCheckBox);
		cbBattery = (CheckBox) findViewById(R.id.batteryCheckBox);
		cbSaveLog = (CheckBox) findViewById(R.id.saveLogCheckBox);
		m_commentTxt  = (EditText) findViewById(R.id.comment);
		m_startStopSwitch = (Switch) findViewById(R.id.start_stop_switch);
		
	    Log.d(Constants.TAG, "Checking whether service is running..");
	    if (isMyServiceRunning(BackgroundRecorder.class)) {
	    	Log.d(Constants.TAG, "Service is running. Trying to bind...");
	    	bindToService();
	    }
	    
	    m_inhandler = new IncomingHandler(this);
	    m_startStopSwitch.setOnCheckedChangeListener(this);
        Log.d(Constants.TAG, "App initialized.");
    }
	
	@Override
	protected void onDestroy()
	{
		if (m_serviceConnection != null) {
			unbindService(m_serviceConnection);
		}
		
		super.onDestroy();
	}

	private void bindToService() {
		m_serviceConnection = new ServiceConnection() {
			
			@Override
			public void onServiceDisconnected(ComponentName name) {
				Log.d(Constants.TAG, "Unbound from service");
				m_serviceConnection = null;
			}
			
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				Log.d(Constants.TAG, "Bound to service");
				BackgroundRecorder recorder = ((BackgroundRecorder.BackgroundRecorderBinder) service).getService();
				m_startStopSwitch.setChecked(recorder.isRunning());
				
				Intent workIntent = recorder.getWorkIntent();
				if (null != workIntent) {
					Log.d(Constants.TAG, "Setting view controls");
					Bundle extras = workIntent.getExtras();
					cbGPS.setChecked(extras.getBoolean(Constants.EXTRA_GPS));
					cbBattery.setChecked(extras.getBoolean(Constants.EXTRA_BATTERY));
					cbSignalStrength.setChecked(extras.getBoolean(Constants.EXTRA_SIGNAL_STRENGTH));
					cbSaveLog.setChecked(extras.getBoolean(Constants.EXTRA_SAVE_LOG));
					m_commentTxt.setText(extras.getString(Constants.EXTRA_COMMENT));
				}
				
				unbindService(m_serviceConnection);
				m_serviceConnection = null;
			}
		};
		
    	bindService(new Intent(this, BackgroundRecorder.class), m_serviceConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onCheckedChanged(CompoundButton btnView, boolean isChecked)
	{
		Log.d(Constants.TAG, String.format("Changed recording state to %s", isChecked ? "on" : "off"));
		
    	if (isChecked) {
    		start();
    	} else {
    		stop();
    	}
	}

	private boolean getExecutionStatus()
	{
		final Switch start_stop_switch = (Switch) findViewById(R.id.start_stop_switch);
		return start_stop_switch.isChecked();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putBoolean(PREF_EXECUTION_STATUS, getExecutionStatus());
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState)
	{
		super.onRestoreInstanceState(savedInstanceState);
		final boolean isRunning = savedInstanceState.getBoolean(PREF_EXECUTION_STATUS, false);
		
		// set switch state to reflect service execution status
		Switch start_stop_switch = (Switch) findViewById(R.id.start_stop_switch);
        start_stop_switch.setChecked(isRunning);
	}
	
	// Start recording
	private void start() {
		Intent startRecIntent;
		
		disableControls();

		// Send command to background service
		startRecIntent = new Intent(App.getAppContext(), BackgroundRecorder.class);
		startRecIntent.putExtra(Constants.EXTRA_GPS, cbGPS.isChecked());
		startRecIntent.putExtra(Constants.EXTRA_SIGNAL_STRENGTH, cbSignalStrength.isChecked());
		startRecIntent.putExtra(Constants.EXTRA_BATTERY, cbBattery.isChecked());
		startRecIntent.putExtra(Constants.EXTRA_SAVE_LOG, cbSaveLog.isChecked());
		startRecIntent.putExtra(Constants.EXTRA_COMMENT, m_commentTxt.getText().toString());
		
		App.getAppContext().startService(startRecIntent);
	}

	private void disableControls() {
		cbGPS.setEnabled(false);
		cbSignalStrength.setEnabled(false);
		cbSaveLog.setEnabled(false);
		m_commentTxt.setEnabled(false);
	}

	// Stop recording
	private void stop() {
		enableControls();
		
		try {
			Thread.sleep(PRE_STOP_WAIT);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		stopService();
	}

	private void enableControls() {
		cbGPS.setEnabled(true);
		cbSignalStrength.setEnabled(true);
		cbSaveLog.setEnabled(true);
		m_commentTxt.setEnabled(true);
	}
	
	private void stopService() {
		App.getAppContext().stopService(new Intent(App.getAppContext(), BackgroundRecorder.class));
	}
	
	public void handleMessage(Message msg)
	{
		Log.v(Constants.TAG, msg.obj.toString());
		m_contentTxt.setText(msg.obj.toString());
	}
	
	static class IncomingHandler extends Handler {
	    private final WeakReference<Main> mainActivity; 

	    IncomingHandler(Main activity) {
	        mainActivity = new WeakReference<Main>(activity);
	    }
	    
	    @Override
	    public void handleMessage(Message msg)
	    {
	         Main activity = mainActivity.get();
	         if (activity != null) {
	              activity.handleMessage(msg);
	         }
	    }
	} // end of IncomingHandler
	
}; // end of Main