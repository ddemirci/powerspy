package com.securitylab.getbatterylevel;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.util.Log;

public class NetworkThread extends Thread {
	
	private static final int THREAD_SLEEP_TIME = 1000;
	private static final String GOOGLE_DNS_SERVER = "8.8.8.8";
	private boolean m_execute = true;
	
	public NetworkThread()
	{
		super("NetworkThread");
	}
	
	public void quit()
	{
		m_execute = false;
	}
	
	public void run()
	{
	    int server_port = 53;
	    byte[] message = new byte[1000];
	    DatagramSocket s;
	    while (m_execute) 
	    {
			try {
				s = new DatagramSocket();
			    InetAddress server = InetAddress.getByName(GOOGLE_DNS_SERVER);
			    DatagramPacket p = new DatagramPacket(message, 1000, server, server_port);
			    s.send(p);
			    
			    try {
			    	Thread.sleep(THREAD_SLEEP_TIME);
			    }
			    catch (InterruptedException e) {
			    	m_execute = false;
			    }
			    
			} catch (Exception e) {
				Log.d(Constants.TAG, e.getMessage());
				e.printStackTrace();
			}
	    }
	}
} // end of NetworkThread