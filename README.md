# Installation
Need to run

$ git submodule init && git submodule update

in order to checkout csvjson.js submodule.

## PowerSpy Android Application
PowerSpy app was used in our experiments for collection the evaluation data.
It enables recording current, voltage, GPS coordinates, signal strength and temperature,
and save it to a local log file.
All the data is saved locally on the device (none is reported over the network).

## Visualization
Open logviewer.html in browser.
Once the map has loaded you can visualize a route on it by
drag-n-dropping a .csv file produced by the GetBatteryLevel app.
